import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement {

    static get properties() {
        return {

            people: { type: Array }
        };
    }

    constructor() {
        super();
        this.people = [];
    }



    render() {
        return html `

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        	

            <h1>
              
            </h1>
            
        
        `;
    }

    updated(changedProperties) {
        console.log("updated en pppersona-stats")
        console.log(changeProperties);


        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-stats")

            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people-stats", {
                detail: {
                    peopleStats: peopleStats
                }
            }))
        }


    }

    gatherPeopleArrayInfo(people) {
        console.log("gatherPeopleArrayInfo en persona-stats")
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;
    }

}



customElements.define('persona-stats', PersonaStats)