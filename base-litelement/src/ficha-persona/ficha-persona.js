import { LitElement,html} from 'lit-element';

class FichaPersona extends LitElement{

    static get properties (){
        return{
            name: {type: String},
            yearsInCompany: {type: Number},
            personaInfo: {type: String},
            photo: {type:Object}    
            
        };
    }

    constructor() {
        // Always calls super() first.  
        super();
    
        this.name = "Elizabeth";
        this.yearsInCompany = 6;
        this.photo = {
            src: "./src/img/persona.jpg",
            alt: "Foto de una persona"			
        };
        this.updatePersonaInfo();
    }

    updated(changedProperties) {

        if (changedProperties.has("name")) {
            console.log("Propiedad name cambio su valor de " + changedProperties.get("name") + " a " + this.name);
        }
        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad YearsInCompany cambio su valor de" + changedProperties.get("yearsInCompany") + " a " + this.yearsInCompany);
        }
        
      }

      updateName(e) {
        console.log("updateName");
        this.name = e.target.value;
      }

      updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
      }        

      updatePersonaInfo(){
          console.log("updatePersonaInfo");
          console.log("yearsInCompany vale" + this.yearsInCompany);

          if(this.yearsInCompany>=7){
            this.personaInfo = "lead";
          } else if (this.yearsInCompany >=5){
            this.personaInfo = "Senior"
          } else if(this.yearsInCompany >=3) {   
           this.personaInfo = "team"
          } else {    
           this.personaInfo = "junior"
          }
        }
    
      



    render() {
        return html `
            <div> 
                    <label for="fname">Nombre Completo</label>
                    <input type="text" id="fname" name="name" value=${this.name} @input=${this.updateName}>
                    </input>
                    <br/>

                    <label for="yearsInCompany">Años en la empresa</label>
                    <input type="text" id="fyearsInCompany" name="yearsInCompany" value=${this.yearsInCompany} @input=${this.updateYearsInCompany}>
                    </input>
                    <br/>

                    <label for="personaInfo">Puesto</label>
                    <input type="text" name="personaInfo" value="${this.personaInfo}" disabled></input>
                    <br/>
                    <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}"></img>
                
            </div>
            `;

        }
    }

   




customElements.define('ficha-persona', FichaPersona)

